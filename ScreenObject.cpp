#include "ScreenObject.h" 
#include "globals.h"

ScreenObject::ScreenObject(objectType type, Model* model, GLuint texture, glm::vec3 position, glm::vec3 hitbox) 
{
	m_type = type;
	m_model = model;
	m_texture = texture;
	m_position = position;
	m_hitbox = hitbox;
	m_modelMatrix = glm::translate(glm::mat4(1.0f), position);
}

void ScreenObject::draw(ShaderHandler::ShaderProgram* shaderProgram)
{	

	glm::mat4 MVPMatrix = gMainCamera->getProjectionMatrix() * gMainCamera->getViewMatrix() * m_modelMatrix;

	//Set active texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_texture);

	//Bind program
	glUseProgram(shaderProgram->programId);

	//Bind Cube
	glBindVertexArray(m_model->VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_model->IBO);

	// Send our transformation to the currently bound shader, 
 	// in the "MVP" uniform
 	glUniformMatrix4fv(shaderProgram->MVPId, 1, GL_FALSE, glm::value_ptr(MVPMatrix));		//Note that glm::value_ptr(gMVPMatrix) is the same as &gMVPMatrix[0][0]]
 	glUniformMatrix4fv(shaderProgram->viewMatrixId, 1, GL_FALSE, glm::value_ptr(gMainCamera->getViewMatrix()));
 	glUniformMatrix4fv(shaderProgram->modelMatrixId, 1, GL_FALSE, glm::value_ptr(m_modelMatrix));


 	//Send in light and color information
 	glUniform4fv(shaderProgram->materialDiffuseColorId, 1, glm::value_ptr(gMaterialDiffuseColor));
 	glUniform4fv(shaderProgram->lightColorId, 1, glm::value_ptr(gLightColor));
 	glUniform4f(shaderProgram->lightPositionId, gLightPosition.x, gLightPosition.y, gLightPosition.z, 1.0f);
 	glUniform4f(shaderProgram->lightDirectionId, gLightDirection.x, gLightDirection.y, gLightDirection.z, 0.0f);
 	//glUniform1f(shaderProgram->lightAttenuationId, 1.0f);	//Strength of the light
 	glUniform1f(shaderProgram->lightAmbientCoefficientId, 0.0f);	//Ambient light
 	glUniform1f(shaderProgram->lightConeAngleId, 0.85);

	//glUniform1i(shaderProgram->textureId, m_texture); 


	glUniform4fv(shaderProgram->materialSpecularColorId, 1, glm::value_ptr(gMaterialSpecularColor));
 	glUniform3fv(shaderProgram->cameraPositionId, 1, glm::value_ptr(gMainCamera->getPosition()));
	//Draw Cube
	//printf("%d - %d, %i\n", gNumIndicesCube, m_model->numberOfIndices, m_type);
	glDrawElements(m_model->drawMode, m_model->numberOfIndices, GL_UNSIGNED_INT, NULL);

	//Unbind program
	glUseProgram(NULL);
}

void ScreenObject::update() {

}

//GET/////////////////////////////////////////
objectType ScreenObject::getType(){
	return m_type;
}
glm::mat4 ScreenObject::getModelMatrix() {
	return m_modelMatrix;
}
glm::vec3 ScreenObject::getPosition() {
	return m_position;
}
glm::vec3 ScreenObject::getHitbox() {
	return m_hitbox;
}

//SET/////////////////////////////////////////
void ScreenObject::setModel(Model* model) {
	m_model = model;
}

void ScreenObject::setModelMatrix(glm::mat4 modelMatrix) {
	m_modelMatrix = modelMatrix;
}

