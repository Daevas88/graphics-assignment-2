#include "Camera.h"
#include "WindowHandler.h"
#include "globals.h"

Camera::Camera(glm::vec3 position) 
{
	m_position = position;
	lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	setProjection(0.1f, 200.0f, glm::quarter_pi<float>() * 1.5);
}

Camera::~Camera(){

}

/**
 * Sets the viewMatrix for the camera
 * @param position 	The position the camera should look at
 * @param up  		The up direction
 */
void Camera::lookAt(glm::vec3 position, glm::vec3 up) {
	m_viewMatrix = glm::lookAt(m_position, position, up);
}

/**
 * Sets the position of the camera
 * @param position 
 */
void Camera::setPosition(glm::vec3 position) {
	m_position = position;
}

/**
 * Sets the perspective matrix for the camera
 * @param near Near clip plane
 * @param far  Far clip plane
 * @param fov  Field of view angle in radians
 */
void Camera::setProjection(float near, float far, float fov) 
{
	m_projectionMatrix = glm::perspective(
		fov,
		static_cast<float>(SCREEN_WIDTH) / static_cast<float>(SCREEN_HEIGHT),
		near,
		far
	);
}
glm::vec3 Camera::getPosition(){
	return m_position;
}
glm::mat4 Camera::getViewMatrix() {
	return m_viewMatrix;
}

glm::mat4 Camera::getProjectionMatrix() {
	return m_projectionMatrix;
}