#pragma once

#include <vector>

#include "ScreenObject.h"
#include "ModelHandler.h"
#include "Level.h"

class Movable : public ScreenObject {
public:
	enum direction {
		UP,
		LEFT,
		DOWN,
		RIGHT,
		NONE
	};

	Movable() {};
	Movable(objectType type, Model* model, GLuint texture, glm::vec3 position, glm::vec3 hitbox) : ScreenObject(type, model, texture, position, hitbox) {};
	~Movable() {};

	void move(Level* currentLevel, float deltaTime);
	void rotate(glm::vec3 direction);
	
	bool handleCollision(Level* currentLevel);
	bool checkCollisionWith(ScreenObject* collidable);
	void resolveCollision(Level* currentLevel, ScreenObject* collidable);

	void setDirection(Movable::direction direction);
	void setSpeed(float speed);

	glm::vec3 getDirection();

private:
	direction m_direction = direction::NONE;
	direction m_oldDirection = direction::NONE;
	glm::vec3 m_movementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 m_oldMovementDirection = glm::vec3(0.0f, 0.0f, 0.0f);
	float m_speed = 0.0f;

};