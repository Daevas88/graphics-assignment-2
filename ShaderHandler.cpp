#include "ShaderHandler.h"

const std::string shaderPath = "./resources/shaders/";

ShaderHandler::ShaderProgram* ShaderHandler::initializeShaders()
{
	this->shaders["Yellow"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("Yellow"));
	this->shaders["SpotLight"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("SpotLight"));
	this->shaders["FullLighting"] = std::unique_ptr<ShaderProgram>(new ShaderProgram("FullLighting"));

	return this->shaders["FullLighting"].get(); 
}

ShaderHandler::ShaderProgram* ShaderHandler::getShader(const std::string& shader)
{
	auto it = this->shaders.find(shader);
	if (it != this->shaders.end())
	{
		return this->shaders[shader].get();
		printf("Could not find shader by name: %s\n", shader.c_str());
	}

	return nullptr;
}

ShaderHandler::ShaderProgram::ShaderProgram(const std::string& shader)
{
	const std::string vertexSuffix = ".vs";
	const std::string fragmentSuffix = ".fs";

	this->programId = LoadShaders((shaderPath + shader + vertexSuffix).c_str(), (shaderPath + shader + fragmentSuffix).c_str());
	
	this->MVPId = glGetUniformLocation(this->programId, "MVP");
	this->viewMatrixId = glGetUniformLocation(this->programId, "ViewMatrix");
	this->modelMatrixId = glGetUniformLocation(this->programId, "ModelMatrix");

	this->textureId = glGetUniformLocation(this->programId, "textureBuffer");

	this->lightPositionId = glGetUniformLocation(this->programId, "LightPosition_worldspace");
	this->lightDirectionId = glGetUniformLocation(this->programId, "lightConeDirection");
	this->lightColorId = glGetUniformLocation(this->programId, "lightColor");
	this->lightAttenuationId = glGetUniformLocation(this->programId, "lightAttenuation");
	this->lightAmbientCoefficientId = glGetUniformLocation(this->programId, "lightAmbientCoefficient");
	this->lightConeAngleId = glGetUniformLocation(this->programId, "lightConeAngle");
	this->lightConeDirectionId = glGetUniformLocation(this->programId, "lightConeDirection");

	this->cameraPositionId = glGetUniformLocation(this->programId, "CameraPosition_worldspace");

	this->materialSpecularColorId = glGetUniformLocation(this->programId, "materialSpecularColor");


	glUseProgram(this->programId);
}

ShaderHandler::ShaderProgram::~ShaderProgram()
{
	glDeleteProgram(this->programId);
}