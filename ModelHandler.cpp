#include "ModelHandler.h"

/**
 * Loads model data from file.
 * @param  name The key for accessing the model from the model map.
 * @param  path Path to file.
 * @return      Success.
 */
bool ModelHandler::loadModelData(std::string name, std::string path) 
{
	int numberOfVertexes;
	int vertexOrder;
	int colorDepth;
	int textureOrder;
	int normalOrder;
	int vertexDataPoints;
	int numberOfIndices;
	int drawMode;
	

	FILE* file = fopen(path.c_str(), "r");
	if(file == NULL) {
		printf("Error!: Could not open the file: %s\n", path.c_str());
		return false;
	}

	fscanf(file, " %d,", &numberOfVertexes);
	fscanf(file, " %d,", &vertexOrder);
	fscanf(file, " %d,", &colorDepth);
	fscanf(file, " %d,", &textureOrder);
	fscanf(file, " %d,", &normalOrder);

	vertexDataPoints = 	numberOfVertexes*vertexOrder+
						numberOfVertexes*colorDepth+
						numberOfVertexes*textureOrder+
						numberOfVertexes*normalOrder;

	std::vector<GLfloat> vertexData;
	for (int i = 0; i < vertexDataPoints; i++) 
	{

		GLfloat temp;
		fscanf(file, " %f", &temp);
		vertexData.push_back(temp);
	}

	fscanf(file, " %d,", &numberOfIndices);

	std::vector<GLuint> indexData;
	for (int i = 0; i<numberOfIndices; i++) 
	{
		GLuint temp;
		fscanf(file, " %d", &temp);
		indexData.push_back(temp);
	}

	fscanf(file, " %d,", &drawMode);

	fclose(file);
	file = nullptr;

	Model model = createModel(drawMode, vertexOrder, colorDepth, textureOrder, normalOrder, vertexData, indexData, numberOfVertexes, numberOfIndices);
	
	//printf("%d, %d, %d, %d, %d, %d, %d\n", numberOfVertexes, vertexOrder, colorDepth, textureOrder, normalOrder, vertexDataPoints, numberOfIndices);
	
	auto response = m_models.insert(std::pair<std::string,Model>(name,model));

	if(response.second == false) 
	{
		printf("Error!: A model by the name of %s already exists.\n", name.c_str());
		return false;
	}
	return true;
}

/**
 * Creates VAO, VBO, IBO and drawMode for the model and store them in a Model struct
 * @param  drawMode			An int representing the OpenGL draw mode.
 * @return                  The finished Model struct
 */
Model ModelHandler::createModel(int drawMode) 
{
	Model model;

	switch(drawMode) 
	{
		case(0)	:	
			model.drawMode = GL_TRIANGLES;		//VAO
			break;
		default: 
			printf("Error!: Draw mode not recognized.\n");
			break;
	}
	return model;
}

Model ModelHandler::createModel(int drawMode, int vertexOrder, int colorDepth, int textureOrder, int normalOrder, std::vector<GLfloat>& vertexData, std::vector<GLuint>& indexData, int numberOfVertexes, int numberOfIndices)
{
	Model model;

	switch(drawMode)
	{
		case(0):	model.drawMode = GL_TRIANGLES;	break;
		case(4):	model.drawMode = GL_TRIANGLE_STRIP; break; 
		default: 
			printf("Error!: Draw mode not recognized.\n");
			break;
	}

	

	//VAO
	glGenVertexArrays(1, &model.VAO);
	glBindVertexArray(model.VAO);

	//Create VBO
	glGenBuffers(1, &model.VBO);
	glBindBuffer(GL_ARRAY_BUFFER, model.VBO);
	glBufferData(GL_ARRAY_BUFFER, vertexData.size()* sizeof(GLfloat), &vertexData.front(), GL_STATIC_DRAW);
	
	//Create IBO
	glGenBuffers(1, &model.IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, model.IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexData.size()* sizeof(GLuint), &indexData.front(), GL_STATIC_DRAW);

	//Enable vertex data
	glEnableVertexAttribArray(0); //attribute 0 is for vertex position data
	glEnableVertexAttribArray(1); //attribute 1 is for vertex color data
	glEnableVertexAttribArray(2); //attribute 2 is for vertex texture data
	glEnableVertexAttribArray(3); //attribute 3 is for vertex normal data

	//Set vertex data
	glVertexAttribPointer(0, vertexOrder, GL_FLOAT, GL_FALSE, vertexOrder * sizeof(GLfloat), NULL);
	glVertexAttribPointer(1, colorDepth, GL_FLOAT, GL_FALSE, colorDepth * sizeof(GLfloat), (void*)(numberOfVertexes*vertexOrder*sizeof(GL_FLOAT))); //color starts after the vertex data
	glVertexAttribPointer(2, textureOrder, GL_FLOAT, GL_FALSE, textureOrder * sizeof(GLfloat), (void*)(numberOfVertexes*vertexOrder*sizeof(GL_FLOAT) + numberOfVertexes * colorDepth*sizeof(GL_FLOAT))); //normal data starts after the color data
	glVertexAttribPointer(3, normalOrder, GL_FLOAT, GL_FALSE, normalOrder * sizeof(GLfloat), (void*)(numberOfVertexes*vertexOrder*sizeof(GL_FLOAT) + numberOfVertexes * colorDepth*sizeof(GL_FLOAT) + numberOfVertexes * normalOrder*sizeof(GL_FLOAT))); //normal data starts after the color data

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	model.numberOfIndices = numberOfIndices;

	return model;
}

Model* ModelHandler::getModel(std::string name) 
{
	if(m_models.count(name)) 
	{
		return &m_models[name];
	} 
	else 
	{
		printf("Error!: No model by name %s could be found.\n", name.c_str());
		return nullptr;
	}
}