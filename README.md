# Assignment 2

# Grade:
Asle: B  
Stefan: E  
David: E  

- Like:
    - Detailed README
- Dislike:
    - The data for the lights might have benefited from a class rather than being globals.

## Group creation deadline 2016/11/11 23:59:59
## Hand in deadline 2016/11/25 23:59:59

In this assignment you will be making Pac Man in 3D.  
This is a group assignment. You will make groups of 3 students (if for some reason this is not possible contact me before the group creation deadline). One of the group members must send an e-mail to me at johannes.hovland2@ntnu.no with the name of the group members by the group creation deadline.

One of the group members will have to **fork**(not clone) this repo. You will be developing on that fork.  
Remember to give Simon and me access so that we can look at what you have done.

There has been som miscomunication between Simon myself and the examination office. The assignments are supposed to be 40% of your grade (not 60% as I though). As such this assignment is counting towards 20% of your grade.  
You will have 3 weeks to finish this assignment.


## Required work
2. Finish the Level::createWalls() function so that walls are created. (The walls should be in 3D)
  2. In addition add a floor to the level.
1. Finish ModelHandler::createModel()
  2. You might wish to change the Model struct.
3. Add textures to the walls. (Any kind of texture will do as long as it is visible. Preferably not yellow.)
3. Finish the Camera class.
  3. The camera should be placed at a 60 degree angle to the level with a perspective view.
4. Finish the Movable::move() function.
  5. Move over any changes you made to the InputHandler in assignment 1 that you think you will need.
5. Create a pacman object from the Movable class or a subclass thereof. (It is ok if pacman is a cube.)
  5. Pacman should be controllable using WASD. He should be able to move in 2D. (You might want to change the Movable::setDirection() function to be more intuative for you.)
  5. Pacman should be yellow
6. Give the packman object a light. The light should have a yellow/redish color. Like from a torch.
  3. Add different kinds of light so that pacman can switch between a pointlight and a spotlight pointing forward by pressing the E key.
7. Create shaders that draw the map in darkness unless pacman is there with his light. The lighting should be full phong lighting and take the color of pacmans light into account. (There is no need to implement shadows. The light can go through the walls.)
2. The code should compile and run on Linux.
1. Update this document. (See bottom.)

## Notes
1. I forgot I promised to add collision to the skeleton code. I'll finish it by the end of the weekend. You are welcome to make your own if you don't want to wait. **Collision code can now be found on the collision branche.**
2. If you have the same issue in your code that exist in the labs where the code will produce a segmentation fault unless being run in debug mode add a comments about that. There will be no penalty if this is the case.
3. The base code has not been tested on Windows.
4. If there are any bugs in the code or other issues please send me an e-mail about it. I have not added any bugs on purpos.

## Suggestions for additional work
1. Implement loding of OBJ files and load and use a model from an OBJ file.
2. Implement a way to move the camera.
4. Port over features from assignment 1. (This will not give as much credit as in asignment 1 unless heavy modification was needed. Add comments about that you needed to change below.)
6. Add a way to switch between perspective and orthographic projection.
3. Add shadows.

##Group comments
###Who are the members of the group?###
Asle Vestly Andersen

Stefan Borgstein

David Eguizabal

###What did you implement and how did you do it? (Individually)###
Stefan + David (his linux broke so we worked on my laptop)
Bunch of small stuff, roughly mixing main code with collision - Asle's changes as we didn't want to remove something usefull accidentaly. Trying to add music, and not sliding when moving but failed misserably :D

Asle:
I started with implementing the drawing and the camera, to get something on the screen to work with. I did this by going through the Lab08 example, porting the code into the assignment as a process to get familiar with it. And then afterwards switch it out piece by piece with the system in the base code.

Finished the importing of the collision code, as I didn't realize the collision code was in its own branch and started working on the main branch. I did some things differently in the modelhandler, as I simply didn't get things to work correctly with the collision code.

Textures, importing the FullLighting shader from Lab08. This part took awhile as I got stuck, misunderstanding some things about how textures work, but Johannes explained me through it. I initially stored the textures as global GLuint ids, but eventually stuffed them into a map in the texture handler. I probably should have moved more globals to different places/structures, but it didn't get prioritized with the time we had.

Got movement to work, this didn't take long as most of it was already part of the base code. At the same time I added a light to pacman that followed him around, which wasn't too difficult either. Just stored its position and fed it to the shader.

Added a floor.model and generated them as tiles.

Spotlight effect, looked at a lot of different shaders and examples. I spent a lot of time making a new shader, trying to mix FullLighting and an example from: http://www.tomdalling.com/blog/modern-opengl/08-even-more-lighting-directional-lights-spotlights-multiple-lights/

It didn't end up working, the code was probably too different from eachother. I ended up using an example from bkAune's group. But I have done some things differently and I hope it shows enough understanding. Implemented it by setting an action when 'e' is pressed, and the action switches the shader.

After setting up the action I also wanted to do the same for the camera, didn't take too long. Set a new camera position, and then after a bit of fiddling I realized I also had to call LookAt each time. Added another action type, another global variable for the view state and that's that.

###What parts if any of the base code did you change and why?###
Stefan + David
Didn't change anything that i can recall off, but if id change anything id make the case system for controls into a bool so you would be able to go diagonally if we were to create a more open level. Add another action type, add another bool.

Asle:
I changed the walls ScreenObject vector in Level to objects, and made it store all objects in the scene(except pacman). 
I also added an type enum to differentiate between them. 

createTextureFromImage and createTexture in TextureHandler was changed to voids, as I stored the textures in a map instead of returning a value.

###What was the hardest part of this assignment? (Individually)###
Stefan + David
Understanding openGL, it was a pretty big challenge getting into the language and then actually implementing stuff as we would have liked.

Asle:
The start, just getting something on the screen. Took awhile to set everything up and understand the program/language to get things going.
Both textures and making a shader for the spotlight share a second place.

###Did you feel like the assignment was an appropriate ammount of work? (Individually)###
Stefan + David
It was alot of work as neither of us have been in touch with anything related to graphics before -the last assignment, so actually getting into it is pretty hard.

Asle:
Relatively so, if everyone contributes equally and extra features is explored and implemented.

###What additional features if any did you add?###
Different views/movement of the camera.

The normal angled view, a top-down view and a first person view that follows pacman. The first person view goes into the wall when colliding, but didn't have time to fix that, and it seems difficult to circumvent with how things are set up.

###Are there any keybindings I should be aware of ouside of WASD?###
e for light changes

q for camera changes
###Other comments###

Asle:

I did most of the work on this alone. We formed our group late, and a short time after the other two went on a week long vacation. The whole situation didn't really leave me motivated so I didn't work that much on it. Until I talked with Johannes in the second week and he told me I could possibly have a personal evaluation instead of a group evaluation. That possibility together with all the help I received from him made the following week a very productive one, managing to complete all the base requirements and an additional feature with some small contributions from the other two. They have spent time on it in this last week, but its mostly been figuring out the code and learning OpenGl. Which was something I did in the first two weeks, even before getting a group I pulled the files and messed around with it.