#pragma	once

#include <string>
#include <vector>
#include <glm/glm.hpp>

#include "ScreenObject.h"
#include "ModelHandler.h"
#include "WindowHandler.h"

class Level 
{
public:
	Level(std::string filepath);

	std::vector<ScreenObject>* getObjects();
	glm::vec2 getMapSize();
	glm::vec3 getStartPos();
	int getScale();
private:
	std::vector<std::vector<int>> map;
	std::vector<ScreenObject> objects;

	glm::vec2 mapSize;
	glm::vec3 pacManStartPos;
	int scale = 2;

	void loadMap(std::string filepath); //Loads map data from file
	void createObjects();	//This should create a ScreenObject for each wall segment.  
};