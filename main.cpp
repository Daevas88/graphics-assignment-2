
#include "globals.h"

#include <stdio.h>
#include <SDL2/SDL.h>
#include <string>

#include "InputHandler.h"
#include "WindowHandler.h"
#include "ShaderHandler.h"
#include "ModelHandler.h"

#include "Level.h"
#include "GameEvent.h"
#include "ScreenObject.h"
#include "Movable.h"
#include "Camera.h"

//Creates a Level object for each line in gLEVELS_FILE and place it it the gLevels vector.
//Those lines are paths of files with map data.
//See Level::loadMap for more information.
//Returns a pointer to the first Level object (currentLevel).
Level* loadLevels() 
{
	FILE* file = fopen(gLEVELS_FILE, "r");

	int f;
	std::string tempString;

	fscanf(file, "Number of files: %d", &f);

	for(int i = 1; i<=f; i++) 
	{
		char tempCString[51];
		fscanf(file, "%50s", &tempCString);
		tempString = tempCString;
		Level level(tempString);
		gLevels.push_back(level);
	}

	fclose(file);
	file = nullptr;
	
	return &gLevels.front();
}

bool initGL()
{
	//Success flag
	bool success = true;

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);
	currentShaderProgram = shaderHandler.initializeShaders();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glClearColor(0.2f, 0.2f, 0.2f, 1.0f);

	return success;
}

/**
 * Initializes the InputHandler, WindowHandler and OpenGL
 */
bool init() 
{
	InputHandler::getInstance().init();

	if(!WindowHandler::getInstance().init()) 
	{
		gRunning = false;
		return false;
	}

	gTextureHandler.createTextureFromImage("floor", "./resources/images/grass.png");
	gTextureHandler.createTextureFromImage("wall", "./resources/images/brick.png");
	gTextureHandler.createTextureFromImage("pacman", "./resources/images/pacman.png");
	gModelHandler.loadModelData("cube", "./resources/models/cube.model");
	gModelHandler.loadModelData("pacman", "./resources/models/pacman.model");
	gModelHandler.loadModelData("floor", "./resources/models/floor.model");

	initGL();

	return true;
}

//While there are events in the eventQueue. Process those events. 
void update(float deltaTime, std::queue<GameEvent>& eventQueue, Movable* pacman, Level* currentLevel) 
{
	GameEvent nextEvent;
	glm::vec3 pacDirection = pacman->getDirection();
	pacDirection *= 2;

	while(!eventQueue.empty()) 
	{
		glm::vec3 pos = gMainCamera->getPosition();
		nextEvent = eventQueue.front();
		eventQueue.pop();

		switch (nextEvent.action) 
		{
		case ActionEnum::PLAYER_MOVE_UP:
			pacman->setDirection(Movable::direction::UP);
			break;
		case ActionEnum::PLAYER_MOVE_LEFT:
			pacman->setDirection(Movable::direction::LEFT);
			break;
		case ActionEnum::PLAYER_MOVE_DOWN:
			pacman->setDirection(Movable::direction::DOWN);
			break;
		case ActionEnum::PLAYER_MOVE_RIGHT:
			pacman->setDirection(Movable::direction::RIGHT);
			break;
		case ActionEnum::PLAYER_FLASHLIGHT:
			if(!spotLightActive)
			{
				currentShaderProgram = shaderHandler.getShader("SpotLight");
				spotLightActive = true;
			}
			else
			{
				currentShaderProgram = shaderHandler.getShader("FullLighting");
				spotLightActive = false;
			}
			break;
		case ActionEnum::PLAYER_SWITCH_VIEW:
			if(gView == 2)
			{
				gMainCamera->setPosition(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y * 0.16, 50.0f));
				gMainCamera->lookAt(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
				gView = 0;
			}
			else if (gView == 0)
			{
				gMainCamera->setPosition(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y, 50.0f));
				gMainCamera->lookAt(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
				gView = 1;
			}
			else if (gView == 1)
			{
				gMainCamera->setPosition(pacman->getPosition() + pacman->getDirection());
				gMainCamera->lookAt(pacman->getPosition() + pacDirection, glm::vec3(0.0f, 0.0f, 1.0f));
				gView = 2;

				if(spotLightActive)
				{
					currentShaderProgram = shaderHandler.getShader("FullLighting");
					spotLightActive = false;
				}
			}
			break;
		default:
			break;
		}
	}
	pacman->move(currentLevel, deltaTime);

	if(spotLightActive)
		gLightPosition = pacman->getPosition();
	else
		gLightPosition = pacman->getPosition() + glm::vec3(0.0f, 0.0f, 5.0f);

	if(gView == 2)
	{
		gMainCamera->setPosition(pacman->getPosition() + pacman->getDirection());
		gMainCamera->lookAt(pacman->getPosition() + pacDirection, glm::vec3(0.0f, 0.0f, 1.0f));
	}
	
	if (pacman->getDirection() != glm::vec3(0.0f, 0.0f, 0.0f))
		gLightDirection = pacman->getDirection();
}

//All draw calls should originate here
void draw(Movable* pacman, Level* currentLevel) 
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	for(auto o : (*currentLevel->getObjects())) {
		o.draw(currentShaderProgram);
	}
	pacman->draw(shaderHandler.getShader("Yellow"));

	//Update screen
	SDL_GL_SwapWindow(WindowHandler::getInstance().getWindow());
}

//Calls cleanup code on program exit.
void close() {
	WindowHandler::getInstance().close();
}

int main(int argc, char *argv[]) 
{
	float nextFrame      = 1/gFpsGoal; //Time between frames in seconds
	float nextFrameTimer = 0.0f; //Time from last frame in seconds
	float deltaTime      = 0.0f; //Time since last pass through of the game loop.
	auto clockStart      = std::chrono::high_resolution_clock::now(); //Clock used for timing purposes
	auto clockStop       = clockStart;

	init();
	
	std::queue<GameEvent> eventQueue; //Main event queue for the program.

	Level* currentLevel = loadLevels();

	Movable pacman(Player, gModelHandler.getModel("pacman"), 0, currentLevel->getStartPos(), glm::vec3(currentLevel->getScale()));
	pacman.setSpeed(5.0f); 

	gMainCamera = new Camera(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y * 0.16, 50.0f));
	gMainCamera->lookAt(glm::vec3(currentLevel->getMapSize().x, currentLevel->getMapSize().y, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
	gLightPosition = gMainCamera->getPosition();

	while(gRunning) 
	{
		clockStart = std::chrono::high_resolution_clock::now();

		InputHandler::getInstance().readInput(eventQueue);
		update(deltaTime, eventQueue, &pacman, currentLevel);

		if(nextFrameTimer >= nextFrame) 
		{
			draw(&pacman, currentLevel);
			nextFrameTimer = 0.0f;
		}

		clockStop = std::chrono::high_resolution_clock::now();
		deltaTime = std::chrono::duration<float, std::chrono::seconds::period>(clockStop - clockStart).count();
		
		nextFrameTimer += deltaTime;
	}

	close();
	return 0;
}