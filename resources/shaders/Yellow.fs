#version 330 core

// Ouput data
out vec4 color;

vec4 fragmentColor = vec4(1.0, 1.0, 0.0, 0.5);

void main()
{

	// Output color = color specified in the vertex shader
	color = fragmentColor;
}
