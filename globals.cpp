#include "globals.h"

const char gWINDOW_CONFIGURATION_FILE[] = "windowConfig";
const char gLEVELS_FILE[] = "levelsList";

bool gRunning = true;
float gFpsGoal = 60.0f;

bool spotLightActive = false;
int gView = 0;

const int SCREEN_WIDTH = 720;
const int SCREEN_HEIGHT = 720;

std::vector<Level> gLevels;

//Graphics program
GLuint gProgramID = 0;

Camera* gMainCamera = nullptr;

//Light
glm::vec3 gLightPosition;
glm::vec3 gLightDirection = glm::vec3(1.0f, 0.0f, 0.0f);
glm::vec4 gLightColor = glm::vec4(0.7f, 0.4f, 0.4f, 1.0f);

ModelHandler gModelHandler;

//Shaders
ShaderHandler shaderHandler;
ShaderHandler::ShaderProgram* currentShaderProgram;

//Textures
TextureHandler gTextureHandler;

//Colors
glm::vec4 gMaterialDiffuseColor = glm::vec4(1.0f, 0.5f, 0.5f, 1.0f);
glm::vec4 gMaterialSpecularColor = glm::vec4(0.5f, 0.5f, 0.5f, 0.8f);
glm::vec4 gBackgroundColor = glm::vec4(0.2f, 0.2f, 0.2f, 1.0f);