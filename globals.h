#pragma	once

#if defined(__linux__)						// If we are using linux.

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>

#elif defined(_WIN16) || defined(_WIN32) || defined(_WIN64)	// If we are using a windows machine
#include <SDKDDKVer.h>

//Using SDL, SDL OpenGL, GLEW, standard IO, and strings
#include <SDL.h>
#include <gl\glew.h>
#include <SDL_opengl.h>
#include <gl\glu.h>
#endif

#include <string>
#include <iostream>
#include <vector>

#include "ScreenObject.h"
#include "TextureHandler.h"
#include "ModelHandler.h"
#include "Camera.h"
#include "Level.h"

extern const char gWINDOW_CONFIGURATION_FILE[];
extern const char gLEVELS_FILE[];

extern bool gRunning;
extern float gFpsGoal;

extern bool spotLightActive;
extern int gView;

//Screen dimension constants
extern const int SCREEN_WIDTH;
extern const int SCREEN_HEIGHT;

extern std::vector<Level> gLevels;

//Graphics program
extern GLuint gProgramID;

extern Camera* gMainCamera;

//Light
extern glm::vec3 gLightPosition;
extern glm::vec3 gLightDirection;
extern glm::vec4 gLightColor;

extern ModelHandler gModelHandler;

//Shaders
extern ShaderHandler shaderHandler;
extern ShaderHandler::ShaderProgram* currentShaderProgram;

//Textures
extern TextureHandler gTextureHandler;

//Material Data Cube
extern glm::vec4 gMaterialDiffuseColor;
extern glm::vec4 gMaterialSpecularColor;

extern glm::vec4 gBackgroundColor;