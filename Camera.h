#pragma once

#define GLM_FORCE_RADIANS //Using degrees with glm is deprecated.
#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

class Camera 
{
public:
	Camera() {};
	Camera(glm::vec3 position);
	~Camera();

	void lookAt(
		glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f)
		);

	void setPosition(glm::vec3 pos);
	void setProjection(float near, float far, float fov = glm::quarter_pi<float>() * 1.5f);

	glm::vec3 getPosition();
	glm::mat4 getViewMatrix();
	glm::mat4 getProjectionMatrix();
private:
	glm::vec3 m_position;

	glm::mat4 m_viewMatrix;
	glm::mat4 m_projectionMatrix;
};