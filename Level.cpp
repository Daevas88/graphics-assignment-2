#include "Level.h"
#include "globals.h"

Level::Level(std::string filepath) 
{
	loadMap(filepath);
	createObjects();
}

/* 
Reads a text file containing map data in the following format:
5x5
1 1 1 1 1
1 0 0 0 1
2 0 1 0 0
1 0 0 0 1
1 1 1 1 1
The first line defines the dimensions of the map
The following matrix has 1s representing walls and 0s representing open space.
2 represents Pac Mans start position.
*/
void Level::loadMap(std::string filepath) 
{
	int x, y, temp;
	FILE* file = fopen(filepath.c_str(), "r");
	
	fscanf(file, "%dx%d", &x, &y);

	mapSize.x = x;
	mapSize.y = y;

	//glm::vec2 screenSize = WindowHandler::getInstance().getScreenSize();

	for(int i = 0; i<y; i++) 
	{
		std::vector<int> row;
		for(int j = 0; j<x; j++) 
		{
			fscanf(file, " %d", &temp);
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;
}

//Uses the map data to create ScreenObjects that represent the walls of the map.
void Level::createObjects() 
{
	Model* cube = gModelHandler.getModel("cube");
	Model* floor = gModelHandler.getModel("floor");
	GLuint floorTexture = gTextureHandler.getTexture("floor");
	GLuint wallTexture = gTextureHandler.getTexture("wall");

	printf("%i, %i", floorTexture, wallTexture);

	for(int i = 0; i < mapSize.y; i++) 
	{
		for(int j = 0; j < mapSize.x; j++) 
		{
			if(map[i][j] == 1) 
			{
				// TODO
				ScreenObject wall(Wall, cube, wallTexture, glm::vec3(j * 2, i * 2, 0.0f), glm::vec3(2.0f,2.0f,2.0f));    
				objects.push_back(wall);
			}
			else if (map[i][j] == 2)
			{
				pacManStartPos = glm::vec3(j*2, i*2, 0);
			}
			if (map[i][j] == 0 || map[i][j] == 2)
			{
				ScreenObject tile(Floor, floor, floorTexture, glm::vec3(j * 2, i * 2, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f));
				objects.push_back(tile);
			}
		}
	}
	//ScreenObject tile(floor, gFloorTexture, glm::vec3(0, 0, 4.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	//objects.push_back(tile);
}

std::vector<ScreenObject>* Level::getObjects() {
	return &objects;
}

glm::vec2 Level::getMapSize() {
	return mapSize;
}

glm::vec3 Level::getStartPos() {
	return pacManStartPos;
}

int Level::getScale() {
	return scale;
}