#pragma once

#include <SDL2/SDL.h>
#include <vector>
#include <glm/glm.hpp>

#include "ScreenObject.h"

class WindowHandler 
{
public:
	//This ensures that there can only be one WindowHandler object at any given time.
	static WindowHandler& getInstance() 
	{
		static WindowHandler instance;
		return instance;
	}

	bool init();
	void close();

	void draw(ScreenObject* object); //This should draw a ScreenObject object or a child class there of.
	void drawList(std::vector<ScreenObject>* objects);
	glm::vec2 getScreenSize();
	SDL_Window* getWindow();

	//This ensures that there can only be one WindowHandler object at any given time.
	WindowHandler(WindowHandler const& copy) = delete;
	void operator=(WindowHandler const& copy) = delete;
private:
	SDL_Window* window;
	SDL_GLContext context;

	char windowName[100];
	int windowXSize;
	int windowYSize;

	WindowHandler() {};

	bool initSDL();
	bool initGL();

	void loadConfig();
};